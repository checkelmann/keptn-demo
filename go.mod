module gitcloud.ert.com/devops/demo/foo-service

go 1.13

require (
	github.com/Unleash/unleash-client-go v0.0.0-20190923201156-aae25c357956
	github.com/Unleash/unleash-client-go/v3 v3.1.1 // indirect
)
